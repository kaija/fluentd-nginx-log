FROM fluent/fluentd:v1.2-onbuild

RUN apk add --update --virtual .build-deps sudo build-base ruby-dev geoip geoip-dev autoconf automake libtool libmaxminddb \
    && sudo gem install fluent-plugin-s3 fluent-plugin-geoip \
    && sudo gem sources --clear-all \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* /home/fluent/.gem/ruby/2.3.0/cache/*.gem

RUN apk add --update geoip

COPY entrypoint.sh /bin/

COPY bin/confd /bin/confd

COPY confd /etc/confd

ENTRYPOINT ["/bin/entrypoint.sh"]

CMD exec fluentd -vv -c /fluentd/etc/${FLUENTD_CONF} -p /fluentd/plugins $FLUENTD_OPT
