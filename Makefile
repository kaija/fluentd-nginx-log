REGISTRY=kaija
REPO=fluentd-nginx-log
TAG=latest


all: build

build:
	mkdir -p plugins
	docker build -t ${REGISTRY}/${REPO}:${TAG} -f Dockerfile .

push:
	docker push ${REGISTRY}/${REPO}:${TAG}

run:
	docker run -it -v /var/log/nginx:/fluentd/log/nginx ${REGISTRY}/${REPO}:${TAG}

compose:
	docker-compose up -d
